@echo off
set mypath=%cd%

if exist "%mypath%\Eco Dedicated Server.pid" (

	set mypid = 0

	FOR /F "tokens=* USEBACKQ" %%F IN (`type "%mypath%\Eco Dedicated Server.pid"`) DO (
		taskkill /F /PID "%%F"
	)
	del "%mypath%\Eco Dedicated Server.pid"

	@echo on
	@echo Server is stopped
) else (
	@echo on
	@echo Server is not running
)