@echo off
set mypath=%cd%

if exist "%mypath%\Eco Dedicated Server.pid" (
	@echo on
	@echo Server is already running
) else (
	condenser -launch
	@echo on
	@echo Server is now running
)